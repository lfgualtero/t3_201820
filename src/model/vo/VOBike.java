package model.vo;

import java.util.Date;

/**
 * Representation of a bike object
 */
public class VOBike implements Comparable<VOBike>{
	private int id;

	public VOBike(int id) {
		super();
		this.id = id;
	}

	public int darId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(VOBike o) {
		if(id > o.darId()){
			return 1;
		}else if(id > o.darId()){
			return -1;
		}
		else
			return 0;
	}

	
}



