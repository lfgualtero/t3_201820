package model.vo;

import java.util.Date;

public class VOStation implements Comparable<VOStation>{
	private int id;
	private String name;
	private String city;
	private double latitude;
	private double longitude;
	private int dpcapacity;
	private String online_date;

	public VOStation(int id, String name, String city, double latitude, double longitude, int dpcapacity, String online_date) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpcapacity = dpcapacity;
		this.online_date = online_date;
	}
	
	public int darId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String darName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String darCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public double darLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double darLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int darDpcapacity() {
		return dpcapacity;
	}

	public void setDpcapacity(int dpcapacity) {
		this.dpcapacity = dpcapacity;
	}

	public String darOnline_date() {
		return online_date;
	}

	public void setOnline_date(String online_date) {
		this.online_date = online_date;
	}
	@Override
	public int compareTo(VOStation  o) {
		if(id > o.darId()){
			return 1;
		}else if(id > o.darId()){
			return -1;
		}
		else
			return 0;
	}



}
