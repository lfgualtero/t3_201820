package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip>{
	
	private int trip_id;
	private String start_time;
	private String end_time;
	private int bikeid;
	private int tripduration;
	private int from_station_id;
	private String from_station_name;
	private int to_station_id;
	private String to_station_name;
	private String usertype;
	private String gender;
	private int birthyear;
	
	
	
	




	public VOTrip(int trip_id, String start_time, String end_time, int bikeid, int tripduration, int from_station_id,
			String from_station_name, int to_station_id, String to_station_name, String usertype, String gender,
			int birthyear) {
		super();
		this.trip_id = trip_id;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeid = bikeid;
		this.tripduration = tripduration;
		this.from_station_id = from_station_id;
		this.from_station_name = from_station_name;
		this.to_station_id = to_station_id;
		this.to_station_name = to_station_name;
		this.usertype = usertype;
		this.gender = gender;
		this.birthyear = birthyear;
	}


	public VOTrip(int trip_id, String start_time, String end_time, int bikeid, int tripduration, int from_station_id,
			String from_station_name, int to_station_id, String to_station_name, String usertype) {
		super();
		this.trip_id = trip_id;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeid = bikeid;
		this.tripduration = tripduration;
		this.from_station_id = from_station_id;
		this.from_station_name = from_station_name;
		this.to_station_id = to_station_id;
		this.to_station_name = to_station_name;
		this.usertype = usertype;
		
	}
	public VOTrip(int trip_id, String start_time, String end_time, int bikeid, int tripduration, int from_station_id,
			String from_station_name, int to_station_id, String to_station_name, String usertype, String gender) {
		super();
		this.trip_id = trip_id;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeid = bikeid;
		this.tripduration = tripduration;
		this.from_station_id = from_station_id;
		this.from_station_name = from_station_name;
		this.to_station_id = to_station_id;
		this.to_station_name = to_station_name;
		this.usertype = usertype;
		this.gender = gender;
	}


	public int getTrip_id() {
		return trip_id;
	}


	public void setTrip_id(int trip_id) {
		this.trip_id = trip_id;
	}


	public String getStart_time() {
		return start_time;
	}


	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}


	public String getEnd_time() {
		return end_time;
	}


	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}


	public int getBikeid() {
		return bikeid;
	}


	public void setBikeid(int bikeid) {
		this.bikeid = bikeid;
	}


	public int getTripSeconds() {
		return tripduration;
	}


	public void setTripduration(int tripduration) {
		this.tripduration = tripduration;
	}


	public int getFrom_station_id() {
		return from_station_id;
	}


	public void setFrom_station_id(int from_station_id) {
		this.from_station_id = from_station_id;
	}


	public String getFrom_station_name() {
		return from_station_name;
	}


	public void setFrom_station_name(String from_station_name) {
		this.from_station_name = from_station_name;
	}


	public String getUsertype() {
		return usertype;
	}


	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public int darBirthyear() {
		return birthyear;
	}


	public void setBirthyear(int birthyear) {
		this.birthyear = birthyear;
	}
	
	public int getTo_station_id() {
		return to_station_id;
	}


	public void setTo_station_id(int to_station_id) {
		this.to_station_id = to_station_id;
	}


	public String getTo_station_name() {
		return to_station_name;
	}


	public void setTo_station_name(String to_station_name) {
		this.to_station_name = to_station_name;
	}


	@Override
	public int compareTo(VOTrip o) {
		if(trip_id > o.getTrip_id()){
			return 1;
		}else if(trip_id < o.getTrip_id()){
			return -1;
		}
		else
		return 0;
	}

}
