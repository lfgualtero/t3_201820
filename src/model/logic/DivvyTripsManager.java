package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IteratorList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;



public class DivvyTripsManager implements IDivvyTripsManager {
	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------
	/**
	 * Queu de stations
	 */
	private Queue<VOStation> stationsQ;
	/**
	 * Stack de stations
	 */
	private Stack<VOStation> stationsS;
	/**
	 * Queu de trips
	 */
	private Queue<VOTrip> tripsQ;
	/**
	 * Stack de trips
	 */
	private Stack<VOTrip> tripsS;



	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------
	public DivvyTripsManager() {
		super();
		stationsQ= new Queue<VOStation>();
		tripsQ= new Queue<VOTrip>();
		stationsS= new Stack<VOStation>();
		tripsS=new Stack<VOTrip>();
	}
	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------
	/**
	 * Cargar estaciones
	 */
	public void loadStations (String stationsFile) {

		String csvFile = stationsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();

			while ((line = br.readLine()) != null) {                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.
				try {

					stationsQ.enqueue(new VOStation(Integer.parseInt(datos[0]), datos[1], datos[2], Double.parseDouble(datos[3]), Double.parseDouble(datos[4]), Integer.parseInt(datos[5]), datos[6]));

					stationsS.push(new VOStation(Integer.parseInt(datos[0]), datos[1], datos[2], Double.parseDouble(datos[3]), Double.parseDouble(datos[4]), Integer.parseInt(datos[5]), datos[6]));


				} catch (NumberFormatException e) {
					e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}


	}

	/**
	 * Carga de los viajes realizadas por las bicicletas, los  viajes realizados deben cargarse en una Pila(Stack) y en una Cola
	 * (Queue) en el orden que aparecen en el archivo.
	 * @param tripsFile ubicacion del archivo.
	 */
	public void loadTrips (String tripsFile) {

		String csvFile = tripsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();


			while ((line = br.readLine()) != null) {                
				String[] datos = line.split(cvsSplitBy);
				//Imprime datos.
				try {



					if(datos.length==10)
					{
						tripsQ.enqueue(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9]));
						tripsS.push(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9]));

					}else if(datos.length==11)
					{
						tripsQ.enqueue(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9], datos[10]));
						tripsS.push(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9], datos[10]));

					}else if(datos.length==12)
					{
						tripsQ.enqueue(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9], datos[10], Integer.parseInt(datos[11])));
						tripsS.push(new VOTrip(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), Integer.parseInt(datos[5]),datos[6], Integer.parseInt(datos[7]), datos[8],datos[9], datos[10], Integer.parseInt(datos[11])));

					}

				} catch (NumberFormatException e) {

					e.printStackTrace();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}


	}

	/**
	 * 
	 * Usa la Cola creada para retornar el nombre de las ultimas �n� estaciones en las que ha estado una bicicleta cuyo id entra como
	 * par�metro. �n� es	un	n�mero que entra como par�metro.
	 * @param n es el numero de las estaciones que se quiere. bikeIdP es la bici que ha pasado por dicha estacion
	 * @return El nombre de las ultimas �n� estaciones en las que ha estado la bicicleta
	 */
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bikeIdP, int n) {
		DoublyLinkedList<String> nuevaLista= new DoublyLinkedList<String>();
		
		int cuenta=n;
		
		Iterator<VOTrip> iter=tripsQ.iterator();

		while(iter.hasNext() && cuenta>0){
			VOTrip trip1=(VOTrip)iter.next() ;

			int bikeId=trip1.getBikeid();

			if(trip1!= null && bikeId==bikeIdP )
			{
				nuevaLista.add(trip1.getTo_station_name());
				cuenta--;
			}
		}


		return nuevaLista;
	}

	/**
	 * 
	 * Usa	la	Pila para	retornar la	informaci�n del Trip	n�mero	�n�	que	termin� en	la estaci�n cuyo	id	entra	como	par�metro. (El	
	 * Trip 1	corresponde	al	 primero	 que	lleg� a	esa	estaci�n,	el	Trip	 2	el	 segundo	 que	lleg� a	esa
	 * estaci�n,	y	as� sucesivamente).
	 * @param n es el numero del Trip que se quiere. stationID es la estacion a la que llego el bike en ese trip
	 * @return La	informaci�n del Trip	n�mero	�n�	que	termin� en	la estaci�n cuyo	id	entra	como	par�metro
	 */

	@Override
	public VOTrip customerNumberN (int stationID, int n) {

		Iterator<VOTrip> t = tripsS.iterator(); 
		int contador = n;

		VOTrip trip = null;

		while(t.hasNext() && contador>0 ) {

			trip=t.next();

			if(trip.getTo_station_id() == stationID) {
				contador--;
			}
		}
		return trip;
	}

}
