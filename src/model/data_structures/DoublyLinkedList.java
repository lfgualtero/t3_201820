package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<E extends Comparable<E>> implements IDoublyLinkedList<E> {

	//=======================================================
	// Atributos
	//=======================================================


	private Node<E> headNode;
	private Node<E> lastNode;


	private int size;

	//=======================================================
	// Constructores
	//=======================================================


	//Creates an empty list
	public DoublyLinkedList(){
		size = 0;
		headNode = null;
		lastNode = null;
	}

	//Creates a list with one node(with element e)
	public DoublyLinkedList(E e) {
		headNode = new Node<E>(e);
		lastNode = headNode;
	}

	//=======================================================
	// M�todos 
	//=======================================================

	//Retorna un iterador ue comienza en el primer nodo de la lista
	@Override
	public Iterator<E> iterator() {
		return new IteratorList<E>(headNode);
	}
	
	//Retorna un iterador ue comienza en el ultimo nodo de la lista
	public Iterator<E> iteratorLastNode(){
		return new IteratorList<E>(lastNode);
	}

	//Adds the element at the beginning of the list
	@Override
	public boolean add(E e) {
		boolean res = false;

		if(e!=null) {
			Node<E> nuevo = new Node<E>(e);
			if(size == 0) {
				headNode = nuevo;
				lastNode = nuevo;
				res=true;
			}
			else {
				headNode.cambiarAnterior(nuevo);
				nuevo.cambiarSiguiente(headNode);
				headNode = nuevo;
			}
			size++;
		}
		else {
			throw new NullPointerException("El elemento es nulo");
		}
		return res;
	}

	//Adds the element at the end of the list
	@Override
	public boolean addAtEnd(E e) {
		boolean res = false;

		if(e!= null) {
			Node<E> nuevo = new Node<E>(e);
			if(size == 0) {
				headNode = nuevo;
				lastNode = nuevo;
				res=true;
			}
			else {
				lastNode.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(lastNode);
				lastNode = nuevo;
				res=true;
			}
			size ++;
		}
		else {
			throw new NullPointerException("El elemento es nulo");
		}
		return res;
	}

	//0 basado
	@Override
	public E getElement(int index) {
		if(index < 0 || index >= size)  
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + size);
		}
		Node<E> actual = headNode; 
		int posActual = 0; 
		while(actual != null && posActual < index)
		{
			actual = actual.darSiguiente();
			posActual ++; 
		}
		return actual.darElemento();
	}

	public Node<E> getHeadNode() {
		return headNode;
	}
	public Node<E> getTailNode() {
		return lastNode;
	}

	@Override
	public E element() {
		return headNode.darElemento();
	}

	@Override
	public int getSize() {
		return size;
	}


	@Override
	public E delete(int index) {
		E eliminado = null;
		Node<E> actual = (Node<E>) this.headNode;

		if(index < 0 || index >= size) {
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + size);
		}

		if(index == 0) {
			eliminado = headNode.darElemento();
			headNode = headNode.darSiguiente();
			this.size --;
		}
		else {
			int contador = 0;
			while(contador + 1 < index){
				actual = (Node<E>) actual.darSiguiente();
				contador++;
			}
			eliminado = actual.darSiguiente().darElemento();
			actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
		
			if(actual.darSiguiente() == null) {
				lastNode = actual;
			}
			else {
				Node<E> c = (Node<E>) actual.darSiguiente();
				c.cambiarAnterior(actual);
			}
			this.size --;
			
		}
		return eliminado;
	}

	@Override
	public boolean isEmpty() {
		return headNode == null;
	}

}
