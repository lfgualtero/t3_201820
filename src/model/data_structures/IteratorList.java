package model.data_structures;

import java.util.Iterator;
//Iterator

public class IteratorList<E> implements Iterator<E>{

	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private Node<E> anterior;

	/**
	 * Nodo en el que se encuentra el iterador.
	 */
	private Node<E> actual;
	
	/**
     * Crea un nuevo iterador de lista iniciando en el nodo que llega por par�metro
     * @param primerNodo el nodo en el que inicia el iterador. nActual != null
     */
	public IteratorList(Node<E> primerNodo)
	{
		actual = primerNodo;
		anterior = null;
	}
	
	
	
    /**
     * Indica si hay nodo siguiente
     * true en caso que haya nodo siguiente o false en caso contrario
     */
	public boolean hasNext() 
	{
		// TODO Completar seg�n la documentaci�n
		return actual.darSiguiente() != null;
	}

    /**
     * Indica si hay nodo anterior
     * true en caso que haya nodo anterior o false en caso contrario
     */
	public boolean hasPrevious() 
	{
		// TODO Completar seg�n la documentaci�n
		return actual.darAnterior() != null;
	}

    /**
     * Devuelve el elemento siguiente de la iteraci�n y avanza.
     * @return elemento siguiente de la iteraci�n
     */
	public E next() 
	{
		// TODO Completar seg�n la documentaci�n
		E elemento = null;
		
		if(actual != null && hasNext()) {
			anterior = actual;
			actual = (Node<E>) actual.darSiguiente();
			elemento = anterior.darElemento();
		}
		else {
			if(actual!=null) {
				elemento = actual.darElemento();
				actual = null;
			}
		}
		return elemento;
	}

    /**
     * Devuelve el elemento anterior de la iteraci�n y retrocede.
     * @return elemento anterior de la iteraci�n.
     */
	public E previous() 
	{
		// TODO Completar seg�n la documentaci�n
		E elemento = null;

		if(actual != null && hasPrevious()) {
			anterior = actual;
			actual = actual.darAnterior();
			elemento = anterior.darElemento();
		}
		else {
			if(actual != null) {
				anterior = actual;
				elemento = anterior.darElemento();
				actual = null;
			}
		}
		return elemento;
	}
	
	
	//=======================================================
	// M�todos que no se implementar�n
	//=======================================================
	
	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(E e) 
	{
		throw new UnsupportedOperationException();
	}
	
	public void add(E e) 
	{
		throw new UnsupportedOperationException();		
	}

}
